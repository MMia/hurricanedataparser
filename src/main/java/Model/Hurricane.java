package Model;

import java.util.LinkedList;
import java.util.List;

public class Hurricane {
    private String name;
    private List<Integer> maxWindSpeed = new LinkedList<>();

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void addMaxWindSpeedValue(int windSpeed){
        maxWindSpeed.add(windSpeed);
    }

    @Override
    public String toString() {
        String output = getName()+": ";
        for (Integer maxWindSpeed : maxWindSpeed){
            output += maxWindSpeed+", ";
        }
        int outputLength = output.length();
        return output.substring(0,outputLength-2);
    }

}
