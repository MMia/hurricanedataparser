import Model.Hurricane;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.util.LinkedList;
import java.util.List;
import java.util.Scanner;

public class DataProvider {
    private static final String FILE_PATH = "hurdat2-nepac-1949-2016-041317.txt";

    private List<Hurricane> hurricanes = new LinkedList<>();

    public void printHurricanes(){
        readFileAndAddHurricanesToList();
        hurricanes.forEach(hurricane -> System.out.println(hurricane.toString()));
    }

    private void readFileAndAddHurricanesToList(){
        try {
            Scanner scanner = new Scanner(new FileReader(getFile()));
            while (scanner.hasNextLine()){
                String actualLine = scanner.nextLine();
                String[] splitActualLine = actualLine.split(",");
                if(splitActualLine.length<=3){
                    Hurricane hurricane = new Hurricane();
                    hurricane.setName(splitActualLine[1].trim());
                    hurricanes.add(hurricane);
                }
                if(splitActualLine.length>4){
                    int position = hurricanes.size();
                    Hurricane hurricane = hurricanes.get(position-1);
                    int windSpeed = Integer.valueOf(splitActualLine[6].trim());
                    hurricane.addMaxWindSpeedValue(windSpeed);
                }
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
    }

    private File getFile() {
        return new File(getClass().getClassLoader().getResource(FILE_PATH).getFile());
    }
}
